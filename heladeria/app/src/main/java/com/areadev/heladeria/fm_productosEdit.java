package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.areadev.heladeria.Controller.productosDAO;
import com.areadev.heladeria.Controller.categoriaDAO;
import com.areadev.heladeria.Entidades.productosHelado;
import com.areadev.heladeria.tools.utils;


public class fm_productosEdit extends AppCompatActivity {

    Button btn_save;
    EditText txt_nombreProducto,date_producto;
    Spinner cmbx_listaproducto;
    TextView txt_panel,txt_id;
    ImageButton btn_deleteProduct;
    productosDAO db = null;
    productosHelado datos;
    //variables de clase
    private boolean modoUpdate=false;
    private String[] listadoCategoria;
    SharedPreferences prefs;
    SharedPreferences.Editor editPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_productos_edit);

        //inicializando elementos
        txt_panel = findViewById(R.id.txt_panelproduct);
        txt_id = findViewById(R.id.txt_idProduct);
        btn_save = findViewById(R.id.btn_saveProduct);
        btn_deleteProduct = findViewById(R.id.btn_deleteItemProduct);
        txt_nombreProducto = findViewById(R.id.edt_nombreProducto);
        date_producto = findViewById(R.id.edt_registroProducto);
        cmbx_listaproducto = findViewById(R.id.cmbx_listaCategoria);

        //obteniendo datos del sharedpreferences
        prefs = this.getSharedPreferences("datosProductos", Context.MODE_PRIVATE);
        this.modoUpdate = prefs.getBoolean("modeUpdate",false);
        //shpf
        editPrefs = prefs.edit();
        //aplicando datos de elementos
        this.fn_AplyComplements();
        this.fn_SetData();

        //rellenando listado de categorias
        categoriaDAO cmb = new categoriaDAO(getApplicationContext());
        this.listadoCategoria = cmb.fn_ListarCategoriasHelados();

        //rellenando cmbx
        cmbx_listaproducto.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                this.listadoCategoria
        ));

        //inciando motor db
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    productosHelado producto = new productosHelado();
                    if(modoUpdate == true){
                        //modo actualizar datos
                        producto.setId(datos.getId());
                        producto.setNombre(txt_nombreProducto.getText().toString().toUpperCase());
                        producto.setCategoria(cmbx_listaproducto.getSelectedItem().toString());
                        producto.setFechaRegistro(date_producto.getText().toString());

                        productosDAO rundata = new productosDAO(getApplicationContext());
                        if(rundata.fn_SaveHelados(producto)){
                            utils.ShowMessages(getApplicationContext(),"Datos Actualizados");
                            fn_clearFields();
                        }else{
                            utils.ShowMessages(getApplicationContext(),"No se guardaron los datos");
                        }
                        //fin actualizar
                    }else{
                        //Guardar elementos
                        producto.setNombre(txt_nombreProducto.getText().toString().toUpperCase());
                        producto.setCategoria(cmbx_listaproducto.getSelectedItem().toString());
                        producto.setFechaRegistro(date_producto.getText().toString());


                        productosDAO rundata = new productosDAO(getApplicationContext());
                        if(rundata.fn_SaveHelados(producto)){
                            utils.ShowMessages(getApplicationContext(),"Datos Guardados");
                            fn_clearFields();
                        }else{
                            utils.ShowMessages(getApplicationContext(),"No se guardaron los datos");
                        }
                        //fin guardar
                    }

                }catch (Exception e){
                    utils.ShowMessages(view.getContext(),"Ex :"+e);
                }
            }
        });

        btn_deleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //elimina el elemento seleccionado
                try {
                    productosHelado producto = new productosHelado();
                    //Guardar elementos
                    producto.setId(txt_id.getText().toString());
                    producto.setNombre(txt_nombreProducto.getText().toString().toUpperCase());
                    producto.setFechaRegistro(date_producto.getText().toString());

                    productosDAO rundata = new productosDAO(getApplicationContext());
                    if(rundata.fn_DeleteHelados(producto)== true){
                        utils.ShowMessages(getApplicationContext(),"Elemento Eliminado");
                        fn_clearFields();
                    }else{
                        utils.ShowMessages(getApplicationContext(),"No se elimino Elemento");
                    }
                }catch (Exception e){
                    utils.ShowMessages(getApplicationContext(),"Ex :"+e);
                }

            }
        });
    }
    //fin oncreate
    private void fn_AplyComplements(){
        //aplicamos la visibilidad y activamos los elementos segun sea el valor de modoUpdate .
        //si modoUpadate es true , entonces vamos a editar el elemento , sino lo guardara
        if (this.modoUpdate == true) {
            txt_panel.setText("Actualizar Elemento");
            btn_deleteProduct.setEnabled(true);
            txt_id.setEnabled(true);
            btn_save.setText("Actualizar");
            this.fn_AplyData();
        }else{
            txt_panel.setText("Agregar Elemento");
            btn_deleteProduct.setEnabled(false);
            txt_id.setEnabled(false);
            btn_save.setText("Guardar");
        }
    }

    public void fn_clearFields(){
        txt_nombreProducto.setText("");
        date_producto.setText("");
    }

    private void fn_AplyData(){
        if(datos !=null){
            txt_id.setText(datos.getId());
            txt_nombreProducto.setText(datos.getNombre());
            date_producto.setText(datos.getFechaRegistro());
            editPrefs.putBoolean("modeUpdate",false);
            editPrefs.putString("id",null);
            editPrefs.putString("nombre",null);
            editPrefs.putString("fecha",null);
            editPrefs.putString("fecha",null);
            editPrefs.commit();
        }
    }


    //metodos de implementacion
    public void fn_SetData(){
        this.datos = new productosHelado();
        this.datos.setId(prefs.getString("id",null));
        this.datos.setNombre(prefs.getString("nombre",null));
        this.datos.setFechaRegistro(prefs.getString("fecha",null));

        this.fn_AplyData();
    }
    //fin clase
}
