package com.areadev.heladeria.Controller;

import android.content.Context;
import android.content.Intent;

import com.areadev.heladeria.Entidades.categoriaHelado;
import com.areadev.heladeria.PersistenceModule.persistencia;
import com.areadev.heladeria.tools.utils;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

//clase que se encargara de las operaciones de CRUD para la entidad Categoria .

public class categoriaDAO {
    persistencia initDB;
    Paper manager;
    private categoriaHelado datosCategoria = null;
    private Context contexto=null;
    private Integer i=0;

    public categoriaDAO(Context cnt) {
        this.contexto = cnt;
        this.manager = persistencia.manager;
    }
    //definiendo los Getters y Setters

    public boolean fn_SaveHelados(categoriaHelado datosValor) {
        //funcion que se encarga de guardar los datos ingresados por el usuario .
        //recibe la entidad CategoriaHelado .
        //retorna : true si los datos fueron guardados , y false si hubo un error o no se pudieron guardar los datos .
        try {
                manager.book("categorias").write(datosValor.getId(), datosValor);
                //verificando

                categoriaHelado verificacion = manager.book("categorias").read(datosValor.getId());
                if (verificacion.getId().isEmpty()) {

                    return false;
                } else {
                    return true;
                }

        } catch (Exception e) {
            utils.ShowMessages(this.contexto, "exeption" + e);
            return false;
        }

    };

    public boolean fn_DeleteHelados(categoriaHelado datosValor) {
        //funcion que se encarga de eliminar los datos ingresados por el usuario .
        //recibe la entidad CategoriaHelado .pero solo es necesario el id de la entidad , mas no todos los datos.
        //retorna : true si los datos fueron eliminados , y false si hubo un error o no se pudieron eliminar los datos .
        try {
            utils.ShowMessages(this.contexto, "Eliminando elemento");

                //ingresando datos
                manager.book("categorias").delete(datosValor.getId());
                return true;
        } catch (Exception e) {
            return false;
        }

    };

    public ArrayList<categoriaHelado> fn_ListarHelados() {
        //funcion que se encarga de listar los datos que se encuentran guardados en los documentos .
        //recibe : nada .
        //retorna : arreglo de datos formateado con la entidad CategoriaHelado.
        ArrayList<categoriaHelado>listaCategoria= new ArrayList<>();
        List<String> allKeys = manager.book("categorias").getAllKeys();

        for (String item : allKeys){

            categoriaHelado itemsData= manager.book("categorias").read(item);

            listaCategoria.add(itemsData);

        }
        return listaCategoria;
    }
    public String[] fn_ListarCategoriasHelados() {
        //funcion que se encarga de listar los datos que se encuentran guardados en los documentos .
        //recibe : nada .
        //retorna : arreglo con el listado de categorias.
        String[] listaCategoria;
        List<String> allKeys = manager.book("categorias").getAllKeys();
        listaCategoria= new String[allKeys.size()];
        this.i = 0;
        for (String item : allKeys){

            categoriaHelado itemsData= manager.book("categorias").read(item);

            listaCategoria[i] = itemsData.getNombreCategoria();
            i++;
        }
        return listaCategoria;
    }

}
