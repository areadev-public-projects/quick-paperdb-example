package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.areadev.heladeria.PersistenceModule.persistencia;


import java.io.File;

public class loginActivity extends AppCompatActivity {
    EditText txt_user,txt_pass;
    Button btn_login,btn_registro;
    String s_user,s_pass;
    SharedPreferences.Editor myEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //iniciando la instancia de PaperDB que sera la misma en toda la aplicacion.
        persistencia.manager.init(this);

        //inciando elementos
        btn_login = findViewById(R.id.btn_ingresar);

        txt_user = findViewById(R.id.edtx_user);
        txt_pass = findViewById(R.id.edtx_pass);

        SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref",MODE_PRIVATE);
        SharedPreferences noteslist = getSharedPreferences("NotesList",MODE_PRIVATE);
        // Creating an Editor object to edit(write to the file)
        myEdit = sharedPreferences.edit();


    };

    public void btn_login_onclick(View view){
        //acciones para el boton de login
        s_user = txt_user.getText().toString();
        s_pass = txt_pass.getText().toString();
        if (s_user.equals("admin") && s_pass.equals("123")){

            //Toast.makeText(getApplicationContext(),"Usuario Correcto",Toast.LENGTH_LONG).show();
            Intent fm_menu = new Intent(view.getContext(),dashActivity.class);
            myEdit.putString("usuario",s_user);
            myEdit.putString("pass",s_pass);
            myEdit.commit();
            startActivity(fm_menu);

        }else{
            //las claves son incorrectas
            Toast.makeText(getApplicationContext(),"Usuario Incorrecto",Toast.LENGTH_LONG).show();
        }
    }



}