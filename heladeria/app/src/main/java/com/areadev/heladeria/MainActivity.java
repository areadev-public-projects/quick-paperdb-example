package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TimerTask tmtsk = new TimerTask() {
            @Override
            public void run() {
                Intent fm_login = new Intent(getApplicationContext(), loginActivity.class);
                startActivity(fm_login);

                finish();
            }
        };

        //incializamos el contador
        Timer tm2 = new Timer();
        tm2.schedule(tmtsk,2000);


    }
    //final metodo inicio


    //final clase
}