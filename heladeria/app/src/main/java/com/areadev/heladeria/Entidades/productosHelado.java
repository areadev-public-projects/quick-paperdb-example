package com.areadev.heladeria.Entidades;

import java.util.UUID;
//clase que modela los datos de las productos.
public class productosHelado {
    private UUID identificador;
    private String id;
    private String Nombre;
    private String Categoria;
    private String FechaRegistro;

    public productosHelado(){
        identificador = identificador.randomUUID();
        this.setId(identificador.toString());
    }
    //aplicando Gettters y Setters


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        FechaRegistro = fechaRegistro;
    }
}
