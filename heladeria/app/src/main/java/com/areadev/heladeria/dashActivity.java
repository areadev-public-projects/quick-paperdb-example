package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class dashActivity extends AppCompatActivity {

    ImageButton btn_cat,btn_prod,btn_exi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);

        //inicializando componentes de la vista
        btn_cat = findViewById(R.id.ibtn_categoria);
        btn_prod = findViewById(R.id.ibtn_productos);
        btn_exi = findViewById(R.id.btn_existencias);

        btn_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fm_categoria = new Intent(getApplicationContext(), datosCategoria.class);
                startActivity(fm_categoria);
            }
        });

        btn_prod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fm_productos = new Intent(getApplicationContext(), datosProductos.class);
                startActivity(fm_productos);
            }
        });

        btn_exi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fm_existencias = new Intent(getApplicationContext(), datosCategoria.class);
                startActivity(fm_existencias);
            }
        });

    }
    //metodos necesarios para el menu
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuprincipal,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_categoria:
                //abrir categoria
                this.fn_openCategoria();
                return true;
            case R.id.menu_productos:
                //abrir categoria
                return true;
            case R.id.menu_existencias:
                //abrir categoria
                return true;
            case R.id.menu_cerrar:
                //abrir categoria
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fn_openCategoria(){
        Intent fm_categoria = new Intent(this, datosCategoria.class);
        startActivity(fm_categoria);
    }
}