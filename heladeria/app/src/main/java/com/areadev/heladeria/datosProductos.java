package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.areadev.heladeria.Controller.productosDAO;
import com.areadev.heladeria.Entidades.productosHelado;
import com.areadev.heladeria.adaptadores.*;
import com.areadev.heladeria.listeners.ItemClickListener;
import com.areadev.heladeria.tools.*;
import java.util.ArrayList;

public class datosProductos extends AppCompatActivity implements ItemClickListener {

    Button btn_addElemento,btn_refsh;
    RecyclerView listaCategorias;
    ArrayList<productosHelado> listadoProductos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_categoria);

        //definiendo lista de datos .
        listadoProductos = new ArrayList<>();
        //inicializando elementos
        btn_addElemento = findViewById(R.id.btn_addproductos);
        btn_refsh = findViewById(R.id.btn_refrescarProductos);

        listaCategorias = findViewById(R.id.lst_productos);
        listaCategorias.setLayoutManager(new LinearLayoutManager(this));
        //llamando a la bd
        this.fn_updateItemList();

        btn_addElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fm_editar = new Intent(getApplicationContext(),fm_productosEdit.class);
                startActivity(fm_editar);
            }
        });

        btn_refsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils.ShowMessages(getApplicationContext(),"Refrescando Datos");
                fn_updateItemList();
            }
        });
    }
    //fin oncreate
    //inicio funciones de clase
    private void fn_updateItemList(){
        try {
            //fn que se encarga de actualizar la lista con los elementos que estan en la BD.
            //retorna : nada
            productosDAO datos = new productosDAO(getApplicationContext());
            //definiendo adaptador
            listadoProductos = datos.fn_ListarHelados();
            if(listadoProductos.size()>=0 || listadoProductos!=null){
                ListaProductosAdapter adaptador = new ListaProductosAdapter(listadoProductos);
                adaptador.setClickListener((ItemClickListener) this);
                listaCategorias.setAdapter(adaptador);

            }else{
                //Toast.makeText(getApplicationContext(),"Sin datos a mostrar",Toast.LENGTH_LONG).show();
                utils.ShowMessages(getApplicationContext(),"Sin datos a mostrar");
            }
        }catch (Exception e){
            // Toast.makeText(getApplicationContext(),"Sin datos a mostrar",Toast.LENGTH_LONG).show();
            utils.ShowMessages(getApplicationContext(),"Ex: "+e);
        }
    }
    //fin funciones de clase

    @Override
    public void onClick(View view, int position) {
        utils.ShowMessages(this,"has presionado boton");
    }
    //fin oncreate func

}