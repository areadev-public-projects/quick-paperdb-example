package com.areadev.heladeria.listeners;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}