package com.areadev.heladeria.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.areadev.heladeria.R;
import com.areadev.heladeria.Entidades.categoriaHelado;
import com.areadev.heladeria.datosCategoria;
import com.areadev.heladeria.fm_categoriaEdit;
import com.areadev.heladeria.listeners.ItemClickListener;
import com.areadev.heladeria.tools.utils;

public class ListaCategoriaAdapter extends RecyclerView.Adapter<ListaCategoriaAdapter.ContactoViewHolder> {
    private ItemClickListener clickListener;
    ArrayList<categoriaHelado> listaCategorias;
    Context contexto;
    SharedPreferences datosCategoria;
    SharedPreferences.Editor myEdit;
    @NonNull
    @Override
    public ContactoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_item_categoria,null,false);

        return new ContactoViewHolder(view);
    }

    public ListaCategoriaAdapter(ArrayList<categoriaHelado> listaCategoria){
        this.listaCategorias=listaCategoria;
    }
    @Override
    public void onBindViewHolder(@NonNull ContactoViewHolder holder, int position) {
        //this.listaContactos=listaContactos;
        holder.txt_id.setText(listaCategorias.get(position).getId());
        holder.txt_categoria.setText(listaCategorias.get(position).getNombreCategoria());
        holder.txt_date.setText(listaCategorias.get(position).getFechaRegistro());

    }
    //configurar el listener
    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return   listaCategorias.size();
    }

    public class ContactoViewHolder extends RecyclerView.ViewHolder{
        TextView txt_id, txt_categoria,txt_date;
        Button bnt_detalle;
        public ContactoViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_id = itemView.findViewById(R.id.txt_id);
            txt_categoria = itemView.findViewById(R.id.txt_nombreCategoria);
            txt_date = itemView.findViewById(R.id.txt_date);
            bnt_detalle = itemView.findViewById(R.id.btn_details);

            bnt_detalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contexto = view.getContext();
                    //utils.ShowMessages(contexto,"datos"+txt_categoria.getText().toString());
                    SharedPreferences sharedPreferences = contexto.getSharedPreferences("datosCategoria",contexto.MODE_PRIVATE);
                    myEdit = sharedPreferences.edit();

                    myEdit.putBoolean("modeUpdate",true);
                    myEdit.putString("id",txt_id.getText().toString());
                    myEdit.putString("categoria",txt_categoria.getText().toString());
                    myEdit.putString("fecha",txt_date.getText().toString());
                    myEdit.commit();
                    Intent fm_editar = new Intent(contexto, fm_categoriaEdit.class);
                    contexto.startActivity(fm_editar);

                }
            });
        }

        // onClick Listener for view

    }
}
