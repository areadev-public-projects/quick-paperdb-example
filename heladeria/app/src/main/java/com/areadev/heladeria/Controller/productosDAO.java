package com.areadev.heladeria.Controller;

import android.content.Context;

import com.areadev.heladeria.Entidades.productosHelado;
import com.areadev.heladeria.PersistenceModule.persistencia;
import com.areadev.heladeria.tools.utils;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

//clase que se encargara de las operaciones de CRUD para la entidad Productos .

public class productosDAO {
    persistencia initDB;
    Paper manager;
    private productosHelado datosCategoria = null;
    private Context contexto=null;


    public productosDAO(Context cnt) {
        this.contexto = cnt;
        this.manager = persistencia.manager;
    }
    //definiendo los Getters y Setters

    public boolean fn_SaveHelados(productosHelado datosValor) {
        //funcion que se encarga de guardar los datos ingresados por el usuario .
        //recibe la entidad productosHelado .
        //retorna : true si los datos fueron guardados , y false si hubo un error o no se pudieron guardar los datos .
        try {
            manager.book("productos").write(datosValor.getId(), datosValor);
            //verificando

            productosHelado verificacion = manager.book("productos").read(datosValor.getId());
            if (verificacion.getId().isEmpty()) {

                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            utils.ShowMessages(this.contexto, "exeption" + e);
            return false;
        }

    };

    public boolean fn_DeleteHelados(productosHelado datosValor) {
        //funcion que se encarga de eliminar los datos ingresados por el usuario .
        //recibe la entidad productosHelado .pero solo es necesario el id de la entidad , mas no todos los datos.
        //retorna : true si los datos fueron eliminados , y false si hubo un error o no se pudieron eliminar los datos .
        try {
            utils.ShowMessages(this.contexto, "Eliminando elemento");

            //ingresando datos
            manager.book("productos").delete(datosValor.getId());
            return true;
        } catch (Exception e) {
            return false;
        }

    };

    public ArrayList<productosHelado> fn_ListarHelados() {
        //funcion que se encarga de listar los datos que se encuentran guardados en los documentos .
        //recibe : nada .
        //retorna : arreglo de datos formateado con la entidad productosHelado.

        ArrayList<productosHelado>listaCategoria= new ArrayList<>();
        List<String> allKeys = manager.book("productos").getAllKeys();

        for (String item : allKeys){

            productosHelado itemsData= manager.book("productos").read(item);

            listaCategoria.add(itemsData);

        }
        return listaCategoria;
    }
}
