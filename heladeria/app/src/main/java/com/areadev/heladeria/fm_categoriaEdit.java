package com.areadev.heladeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.areadev.heladeria.Controller.categoriaDAO;
import com.areadev.heladeria.Entidades.categoriaHelado;
import com.areadev.heladeria.tools.utils;


public class fm_categoriaEdit extends AppCompatActivity {

    Button btn_save;
    EditText nmbCategoria,dtCategoria;
    TextView txt_panel,txt_id;
    ImageButton btn_deleteItem;
    categoriaDAO db = null;
    categoriaHelado datos;
    //variables de clase
    private boolean modoUpdate=false;
    SharedPreferences prefs;
    SharedPreferences.Editor editPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_categoria_edit);

        //inicializando elementos
        txt_panel = findViewById(R.id.txt_panelproduct);
        txt_id = findViewById(R.id.txt_idProduct);
        btn_save = findViewById(R.id.btn_saveProduct);
        btn_deleteItem = findViewById(R.id.btn_deleteItemProduct);
        nmbCategoria = findViewById(R.id.edt_nombreProducto);
        dtCategoria = findViewById(R.id.edt_registroProducto);

        //obteniendo datos del sharedpreferences
        prefs = this.getSharedPreferences("datosCategoria", Context.MODE_PRIVATE);
        this.modoUpdate = prefs.getBoolean("modeUpdate",false);
        //shpf
        editPrefs = prefs.edit();
        //aplicando datos de elementos
        this.fn_AplyComplements();
        this.fn_SetData();

        //inciando motor db
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    categoriaHelado categoria = new categoriaHelado();
                    if(modoUpdate == true){
                        //modo actualizar datos
                        categoria.setId(datos.getId());
                        categoria.setNombreCategoria(nmbCategoria.getText().toString().toUpperCase());
                        categoria.setFechaRegistro(dtCategoria.getText().toString());

                        categoriaDAO rundata = new categoriaDAO(getApplicationContext());
                        if(rundata.fn_SaveHelados(categoria)){
                            utils.ShowMessages(getApplicationContext(),"Datos Actualizados");
                            fn_clearFields();
                        }else{
                            utils.ShowMessages(getApplicationContext(),"No se guardaron los datos");
                        }
                        //fin actualizar
                    }else{
                        //Guardar elementos
                        categoria.setNombreCategoria(nmbCategoria.getText().toString().toUpperCase());
                        categoria.setFechaRegistro(dtCategoria.getText().toString());

                        categoriaDAO rundata = new categoriaDAO(getApplicationContext());
                        if(rundata.fn_SaveHelados(categoria)){
                            utils.ShowMessages(getApplicationContext(),"Datos Guardados");
                            fn_clearFields();
                        }else{
                            utils.ShowMessages(getApplicationContext(),"No se guardaron los datos");
                        }
                        //fin guardar
                    }

                }catch (Exception e){
                    utils.ShowMessages(view.getContext(),"Ex :"+e);
                }
            }
        });

        btn_deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //elimina el elemento seleccionado
                try {
                    categoriaHelado categoria = new categoriaHelado();
                    //Guardar elementos
                    categoria.setId(txt_id.getText().toString());
                    categoria.setNombreCategoria(nmbCategoria.getText().toString().toUpperCase());
                    categoria.setFechaRegistro(dtCategoria.getText().toString());

                    categoriaDAO rundata = new categoriaDAO(getApplicationContext());
                    if(rundata.fn_DeleteHelados(categoria)== true){
                        utils.ShowMessages(getApplicationContext(),"Elemento Eliminado");
                        fn_clearFields();
                    }else{
                        utils.ShowMessages(getApplicationContext(),"No se elimino Elemento");
                    }
                }catch (Exception e){
                        utils.ShowMessages(getApplicationContext(),"Ex :"+e);
                }

            }
        });
    }
    //fin oncreate
    private void fn_AplyComplements(){

        //aplicamos la visibilidad y activamos los elementos segun sea el valor de modoUpdate .
        //si modoUpadate es true , entonces vamos a editar el elemento , sino lo guardara
        if (this.modoUpdate == true) {
            txt_panel.setText("Actualizar Elemento");
            btn_deleteItem.setEnabled(true);
            txt_id.setEnabled(true);
            btn_save.setText("Actualizar");
            this.fn_AplyData();
        }else{
            txt_panel.setText("Agregar Elemento");
            btn_deleteItem.setEnabled(false);
            txt_id.setEnabled(false);
            btn_save.setText("Guardar");
        }
    }

    private void fn_AplyData(){
        if(datos !=null){
            txt_id.setText(datos.getId());
            nmbCategoria.setText(datos.getNombreCategoria());
            dtCategoria.setText(datos.getFechaRegistro());
            editPrefs.putBoolean("modeUpdate",false);
            editPrefs.putString("id",null);
            editPrefs.putString("categoria",null);
            editPrefs.putString("fecha",null);
            editPrefs.commit();
        }
    }

    public void fn_clearFields(){
        //fn que se encarga de limpiar el contenido de los txtbx
        nmbCategoria.setText("");
        dtCategoria.setText("");

    }

    //metodos de implementacion
    public void fn_SetData(){
        this.datos = new categoriaHelado();
        this.datos.setId(prefs.getString("id",null));
        this.datos.setNombreCategoria(prefs.getString("categoria",null));
        this.datos.setFechaRegistro(prefs.getString("fecha",null));

        this.fn_AplyData();
    }
    //fin clase
}
