package com.areadev.heladeria.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.areadev.heladeria.R;
import com.areadev.heladeria.Entidades.productosHelado;
import com.areadev.heladeria.datosProductos;
import com.areadev.heladeria.fm_productosEdit;
import com.areadev.heladeria.listeners.ItemClickListener;
import com.areadev.heladeria.tools.utils;

public class ListaProductosAdapter extends RecyclerView.Adapter<ListaProductosAdapter.ContactoViewHolder> {
    private ItemClickListener clickListener;
    ArrayList<productosHelado> listaProductos;
    Context contexto;
    SharedPreferences datosProductos;
    SharedPreferences.Editor myEdit;
    @NonNull
    @Override
    public ContactoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_item_productos,null,false);

        return new ContactoViewHolder(view);
    }

    public ListaProductosAdapter(ArrayList<productosHelado> lista_productos){
        this.listaProductos=lista_productos;
    }
    @Override
    public void onBindViewHolder(@NonNull ContactoViewHolder holder, int position) {

        holder.txt_id.setText(listaProductos.get(position).getId());
        holder.txt_nombre.setText(listaProductos.get(position).getNombre());
        holder.txt_categoria.setText(listaProductos.get(position).getCategoria());
        holder.txt_dateproduct.setText(listaProductos.get(position).getFechaRegistro());

    }
    //configurar el listener
    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return   listaProductos.size();
    }

    public class ContactoViewHolder extends RecyclerView.ViewHolder{
        TextView txt_id,txt_nombre,txt_categoria,txt_dateproduct;
        ImageButton btn_item;
        public ContactoViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_id = itemView.findViewById(R.id.txt_prdid_categoria);
            txt_nombre = itemView.findViewById(R.id.txt_prd_nombre);
            txt_categoria = itemView.findViewById(R.id.txt_prd_categoria);
            txt_dateproduct = itemView.findViewById(R.id.txt_dateproduct);

            btn_item = itemView.findViewById(R.id.btn_productoItem);

            btn_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contexto = view.getContext();
                    //utils.ShowMessages(contexto,"datos"+txt_categoria.getText().toString());
                    SharedPreferences sharedPreferences = contexto.getSharedPreferences("datosProductos",contexto.MODE_PRIVATE);
                    myEdit = sharedPreferences.edit();

                    myEdit.putBoolean("modeUpdate",true);
                    myEdit.putString("id",txt_id.getText().toString());
                    myEdit.putString("categoria",txt_categoria.getText().toString());
                    myEdit.putString("nombre",txt_nombre.getText().toString());
                    myEdit.putString("fecha",txt_dateproduct.getText().toString());

                    myEdit.commit();
                    Intent fm_editar = new Intent(contexto, fm_productosEdit.class);
                    contexto.startActivity(fm_editar);

                }
            });
        }

        // onClick Listener for view

    }
}
