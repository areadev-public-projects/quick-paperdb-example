package com.areadev.heladeria.Entidades;
import java.util.UUID;

//clase que modela los datos de las categorias de helados .

public class categoriaHelado{
    private UUID identificador;
    private String id;
    private String NombreCategoria;
    private String fechaRegistro;

    //definiendo los Getters y Setters
    public categoriaHelado(){
        identificador = identificador.randomUUID();
        this.setId(identificador.toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreCategoria() {
        return NombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        NombreCategoria = nombreCategoria;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        //obtener la fecha
        /*
        * LocalDate todaysDate = LocalDate.now();
        System.out.println(todaysDate);*/
        this.fechaRegistro = fechaRegistro;
    }
}
