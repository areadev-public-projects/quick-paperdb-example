package com.areadev.heladeria.adaptadores;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.areadev.heladeria.R;
import com.areadev.heladeria.Entidades.categoriaHelado;

public class PlantillaAdapter extends RecyclerView.Adapter<PlantillaAdapter.ContactoViewHolder> {

    ArrayList<categoriaHelado> listaCategorias;
    @NonNull
    @Override
    public ContactoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_item_categoria,null,false);
        return new ContactoViewHolder(view);
    }

    public PlantillaAdapter(ArrayList<categoriaHelado> listaCategoria){
        this.listaCategorias=listaCategoria;
    }
    @Override
    public void onBindViewHolder(@NonNull ContactoViewHolder holder, int position) {
        //this.listaContactos=listaContactos;
        holder.txt_id.setText(listaCategorias.get(position).getId());
        holder.txt_categoria.setText(listaCategorias.get(position).getNombreCategoria());

    }

    @Override
    public int getItemCount() {
        return   listaCategorias.size();
    }

    public class ContactoViewHolder extends RecyclerView.ViewHolder {
        TextView txt_id, txt_categoria;

        public ContactoViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_id = itemView.findViewById(R.id.txt_id);
            txt_categoria = itemView.findViewById(R.id.txt_nombreCategoria);

        }
    }
}

